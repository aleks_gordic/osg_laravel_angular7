<section id="profiles">
    <div class="container">
        <h2><small>check out our</small> profiles</h2>
        <div class="stars">
            <img src="images/stars.png" class="img-fluid d-block mx-auto" alt="">
        </div>
        <div class="row mt-5">
            <div class="col-12 col-lg-4 col-md-4 col-sm-12 mb-5 mb-md-0">
                <div class="profile strength-athlete">
                    <div class="description">
                        <div class="icon"></div>
                        <h3>strength athlete</h3>
                        <ul>
                            <li>Performance Graph</li>
                            <li>Performance Table</li>
                            <li>Top Training Designs</li>
                            <li>Calendar & Training Log</li>
                            <li>Timeline</li>
                        </ul>
                        <div class="text-center"><a href="{{route('register', ['strengthAthlete'])}}" class="btn btn-green-gradient py-3 px-4 mr-0 mr-md-4">sign up</a></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-md-4 col-sm-12 mb-5 mb-md-0">
                <div class="profile program-designer">
                    <div class="description">
                        <div class="icon"></div>
                        <h3>program designer</h3>
                        <ul>
                            <li>Calendar Builder</li>
                            <li>Progression Builder</li>
                            <li>Program Designs</li>
                            <li>Program Publisher</li>
                            <li>Experience Bank</li>
                        </ul>
                        <div class="text-center"><a href="{{route('register', ['programDesigner'])}}" class="btn btn-green-gradient py-3 px-4 mr-0 mr-md-4">sign up</a></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 col-md-4 col-sm-12 mb-5 mb-md-0">
                <div class="profile personal-trainer">
                    <div class="description">
                        <div class="icon"></div>
                        <h3>personal trainer</h3>
                        <ul>
                            <li>My Offers</li>
                            <li>My Client Results</li>
                            <li>Booking</li>
                            <li>Experience Bank & Reviews</li>
                        </ul>
                        <div class="text-center"><a href="{{route('register', ['personalTrainer'])}}" class="btn btn-green-gradient py-3 px-4 mr-0 mr-md-4">sign up</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>