import { Component, OnInit, Input } from '@angular/core';
import { ApplicationUser } from 'src/app/core/classes/user';

@Component({
  selector: 'osg-strength-athlete-performance-table',
  templateUrl: './performance-table.component.html'
})
export class StrengthAthletePerformanceTableComponent implements OnInit{
  
  @Input() applicationUser: ApplicationUser;

  comparisons: string[] = ['bench press', 'squats', 'deadlift'];
  results: Array<{title: string, type: string, value: number, color: string}> = [
    {title:'program', type:'program', value:0, color: ''},
    {title:'bench press', type:'bench', value:8, color: 'blue'},
    {title:'squat', type:'squat', value:45, color: 'orange'},
    {title:'deadlift', type:'dead', value:57, color: 'green'}
  ];
  sliders: Array<{title: string, value: number, color: string}> = [
    {title:'sleep', value:8, color: 'purple'},
    {title:'stress press', value:6, color: 'red'},
    {title:'energy', value:8, color: 'orange'}
  ];

  ngOnInit(): void {
    
  }

}
