import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute  } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'osg-workout-detail',
  templateUrl: './main.component.html'
})
export class WorkoutDetailComponent implements OnInit{
  date: string;
  status: string;
  autoRegulation: boolean;  
  marginTop: string;
  
  isShowingComplete: boolean;
  // values for progress form
  time: string;
  reps: number;
  rtf: number;
  load: number;
  isStart: boolean = false;

  constructor(private activeRoute: ActivatedRoute, private router: Router) { 
    this.activeRoute.paramMap.subscribe(params => {
      this.date = params.get('date');
      this.status = params.get('status');
    });
    this.autoRegulation = true;
    this.isShowingComplete = false;
  }

  ngOnInit(): void {
    this.time = "02:00";
    if (this.status == "completed") {
      this.reps = 8;
      this.rtf = 1;
      this.load = 100;
    } else {
      this.isStart = true;
    }
    
  }

  ngAfterViewInit() {
    window.scrollTo(0, 0);
  }

  valueChange(event) {
    this.reps = event.reps;
    this.rtf = event.rtf;
    this.load = event.load;
  }

  FinishWorkout() {
    this.status = 'completed';
  }

  Back() {
    this.router.navigate(['/workout-log']);
  }

  goProgress() {
    if (this.status != "completed" && this.status != 'missed') {
      this.status = "inprogress";
      this.reps = this.rtf = this.load = 0;
      this.isStart = false;
    }
  }

  viewDetails() {
    this.isShowingComplete = true;
  }

  SkipSet() {
    this.status = "upcoming";
  }

  FinishSet() {
    this.time = "00:00";
    if (this.reps > 7 && this.rtf > 1 && this.load > 100) {
      this.status = "completed";
    } else {
      this.status = "inprogress";
    }
  }
}
