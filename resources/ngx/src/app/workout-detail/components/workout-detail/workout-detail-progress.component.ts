import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'osg-workout-detail-progress',
  templateUrl: './workout-detail-progress.component.html'
})
export class WorkoutDetailProgressComponent implements OnInit{
  @Output() valueChange = new EventEmitter();
  @Input() reps: number = 8;
  @Input() rtf: number = 1;
  @Input() load: number = 100;

  ngOnInit(): void {}

  IncraseRTF() {
    if (this.rtf < 5) this.rtf ++;
    let obj = {
      reps: this.reps,
      rtf: this.rtf,
      load: this.load
    };
    this.valueChange.emit(obj);
  }

  DecraseRTF() {
    if (this.rtf > 0) { this.rtf --; }
    let obj = {
      reps: this.reps,
      rtf: this.rtf,
      load: this.load
    };
    this.valueChange.emit(obj);
  }

  IncraseREPS() {
    this.reps ++;
    this.load += 2.5;
    let obj = {
      reps: this.reps,
      rtf: this.rtf,
      load: this.load
    };
    this.valueChange.emit(obj);
  }

  DecraseREPS() {
    this.reps --;
    this.load -= 2.5;
    let obj = {
      reps: this.reps,
      rtf: this.rtf,
      load: this.load
    };
    this.valueChange.emit(obj);
  }
}
