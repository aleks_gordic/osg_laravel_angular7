<?php

namespace App\Services;

use App\Models\User;
use App\Models\Trainers;
use App\Models\Designers;
use App\Models\Athletes;
use Illuminate\Support\Facades\Hash;

abstract class UserRole{
    const PersonalTrainer = 'PersonalTrainer';
    const ProgramDesigner = 'ProgramDesigner';
    const StrengthAthlete = 'StrengthAthlete';
}

class UserService 
{
    private $roles = [UserRole::PersonalTrainer, UserRole::ProgramDesigner, UserRole::StrengthAthlete];

    public function getAll()
    {
        return User::all();
    }

    public function register(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        $user->role = $this->getRole($data['role']);
        $user->api_token = str_random(60);
        $user->save();

        if ($user->role == "PersonalTrainer") {
            $trainers = Trainers::create([
                'user_id' => $user->id,
                'gender' => $data['per_gender'],
                'birth' => $data['per_birth'],
                'education' => $data['per_education'],
                'special' => $data['per_special'],
                'experience' => $data['per_experience'],
                'country' => $data['per_country'],
                'location' => $data['per_location'],
                'workplace_address' => $data['per_address'],
                'place' => $data['per_place'],
                'postal_code' => $data['per_postal'],
                'description' => $data['per_description']
            ]);
            if (isset($data['avatar'])) {
                $trainers->avatar = $data['avatar'];
                $trainers->save();
            }
        } else if ($user->role == "ProgramDesigner") {
            $desingers = Designers::create([
                'user_id' => $user->id,
                'gender' => $data['pro_gender'],
                'birth' => $data['pro_birth'],
                'education' => $data['pro_education'],
                'special' => $data['pro_special'],
                'experience' => $data['pro_experience'],
                'country' => $data['pro_country'],
                'place' => $data['pro_place'],
                'postal_code' => $data['pro_postal'],
                'description' => $data['pro_description']
            ]);
            if (isset($data['avatar'])) {
                $desingers->avatar = $data['avatar'];
                $desingers->save();
            }
        } else {
            $atheletes = Athletes::create([
                'user_id' => $user->id,
                'gender' => $data['str_gender'],
                'birth' => $data['str_birth'],
                'height' => $data['str_height'],
                'muscle_type' => $data['str_muscle'],
                'interest' => $data['str_interest'],
                'experience' => $data['str_experience'],
                'country' => $data['str_country'],
                'place' => $data['str_place'],
                'postal_code' => $data['str_postal'],
                'description' => $data['str_description']
            ]);
            if (isset($data['avatar'])) {
                $atheletes->avatar = $data['avatar'];
                $atheletes->save();
            }
        }
       
        return $user;
    }

    private function getRole($id){
        return $this->roles[intval($id)];
    }

}
